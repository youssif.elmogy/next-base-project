import { Fragment } from "react";
import Head from "next/head";
import Link from "next/link";
import Todos from "@/components/todos/todos";

function HomePage() {
  return (
    <Fragment>
      <Head>
        <title>Base Project</title>
        <link rel="icon" href="/icons/axalogo.svg" />
      </Head>
      <h2>
        Hello World!
        <Link href="/landing">Landing Page</Link>
      </h2>
      <Todos />
    </Fragment>
  );
}

export default HomePage;
