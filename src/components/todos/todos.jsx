import { todoState } from "@/recoil/atoms/todoState";
import React, { useState } from "react";
import { useRecoilState } from "recoil";
import Todo from "./todo";

const Todos = () => {
  const [todos, setTodos] = useRecoilState(todoState);
  const [inputText, setInputText] = useState("");
  const addTodo = () => {
    setTodos((prevTodos) => [
      ...prevTodos,
      { id: Math.random(), text: inputText },
    ]);
    setInputText("");
  };
  return (
    <main>
      <input
        type="text"
        value={inputText}
        onChange={(e) => setInputText(e.target.value)}
      />
      <button onClick={addTodo}>Add Todo</button>
      <button onClick={() => setTodos([])}>Clear Todos</button>
      {todos.map((todo) => {
        return <Todo key={todo.id} id={todo.id} text={todo.text} />;
      })}
    </main>
  );
};

export default Todos;
